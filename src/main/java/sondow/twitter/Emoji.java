package sondow.twitter;

public class Emoji {

    private final int index;
    private final String literal;
    private final String name;
    private final int score;
    private final Age age;

    public Emoji(int index, String literal, String name) {
        this(index, literal, name, 1, Age.ESTABLISHED);
    }

    public Emoji(int index, String literal, String name, int score) {
        this(index, literal, name, score, Age.ESTABLISHED);
    }

    public Emoji(int index, String literal, String name, int score, Age age) {
        this.index = index;
        this.literal = literal;
        this.name = name;
        this.score = score;
        this.age = age;
    }

    public int getIndex() {
        return index;
    }

    public String getLiteral() {
        return literal;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public Age getAge() {
        return age;
    }
}
