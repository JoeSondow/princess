package sondow.twitter;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Greetings {

    private List<String> list = new ArrayList<>();

    private SecureRandom random;


    public Greetings(SecureRandom random) {
        this.random = random;
        fill();
    }

    private void fill() {
        list.add("good morrow. i'm");
        list.add("kneel before me. i'm");
        list.add("bow down to me, for i am");
        list.add("well be with you. i am");
        list.add("well met, stranger. i'm");
        list.add("how do you do. i am");
        list.add("pleased to meet you. i'm");
        list.add("charmed. i'm");
        list.add("greetings, i'm");
        list.add("good even, i'm");
        list.add("loyal subjects, i'm");
        list.add("it's an honor. i'm");
        list.add("as you were. i'm");
        list.add("blessings upon you. i'm");
    }

    public String random() {
        int index = random.nextInt(list.size());
        return list.get(index);
    }
}
