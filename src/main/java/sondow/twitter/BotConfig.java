package sondow.twitter;

/**
 * The place to store all the bot's configuration variables, mostly from environment variables read
 * in BotConfigFactory.
 */
public class BotConfig {

    /**
     * The configuration for Mastodon.
     */
    private MastodonConfig mastodonConfig;

    public BotConfig(MastodonConfig mastodonConfig) {
        this.mastodonConfig = mastodonConfig;
    }

    public MastodonConfig getMastodonConfig() {
        return mastodonConfig;
    }
}
