package sondow.twitter;

import java.security.SecureRandom;

/**
 * The main application logic.
 */
public class Bot {


    /**
     * The object that does the Mastodon tooting.
     */
    private Tooter tooter;

    private SecureRandom random;

    /**
     * Constructs a new bot with the specified Tweeter, for unit testing.
     */
    Bot(Tooter tooter, SecureRandom random) {
        this.tooter = tooter;
        this.random = random;
    }

    /**
     * Constructs a new bot.
     */
    Bot() {
        this.random = new SecureRandom();
        BotConfig botConfig = new BotConfigFactory().configure();
        MastodonConfig mastodonConfig = botConfig.getMastodonConfig();
        if (mastodonConfig != null) {
            this.tooter = new Tooter(mastodonConfig);
        }
    }

    /**
     * Performs the application logic.
     *
     * @return the posted tweet
     */
    public void go() {
        EmojiSet emojiSet = new EmojiSet(random);
        Emoji emoji = emojiSet.random();
        SkinColor skinColor = emojiSet.randomSkinColor();
        String greeting = new Greetings(random).random();
        Princess princess = new Princess(greeting, emoji, skinColor);
        String message = princess.toString();
        Post post = new Post("Emoji art", message);
        if (tooter != null) {
            tooter.toot(post);
        }
    }

    public static void main(String[] args) {
        new Bot().go();
    }
}
