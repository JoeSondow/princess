package sondow.twitter;

/**
 * Builds a BotConfig based on environment variables.
 */
public class BotConfigFactory {

    private Environment environment;

    public BotConfigFactory(Environment environment) {
        this.environment = environment;
    }

    public BotConfigFactory() {
        this(new Environment());
    }

    public BotConfig configure() {
        MastodonConfig mastodonConfig = configureMastodon();

        return new BotConfig(mastodonConfig);
    }

    /**
     * @return configuration containing Mastodon authentication strings and other variables
     */
    private MastodonConfig configureMastodon() {

        MastodonConfig mastodonConfig = null;

        String credentialsCsv = environment.get("cred_mastodon");
        if (credentialsCsv != null) {
            String[] tokens = credentialsCsv.split(",");
            String instanceName = tokens[0];
            String screenName = tokens[1];
            String accessToken = tokens[2];
            mastodonConfig = new MastodonConfig(instanceName, accessToken);
        }
        return mastodonConfig;
    }
}
