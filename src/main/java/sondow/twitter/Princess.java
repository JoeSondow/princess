package sondow.twitter;

public class Princess {

    private final String greeting;
    private final Emoji emoji;
    private final SkinColor skinColor;

    public Princess(String greeting, Emoji emoji, SkinColor skinColor) {
        this.greeting = greeting;
        this.emoji = emoji;
        this.skinColor = skinColor;
    }

    public String toString() {
        //        return "⠀ ⠀ ⠀  \uD83D\uDC78         \uD83E\uDD1A\n" +
        //                "　   ✨✨✨✨\n" +
        //                "    ✨   ✨\n" +
        //                "   \uD83D\uDC47   ✨✨ \n" +
        //                "  　 ✨✨✨\n" +
        //                "　 ✨✨✨✨\n" +
        //                "　   \uD83D\uDC61     \uD83D\uDC61 \n" +
        //                "good morrow. i'm the princess of sparkles";
        String e = emoji.getLiteral();
        String downwardPointingHand = skinColor.getDownwardPointingHand();
        String raisedHand = skinColor.getRaisedHand();
        String princessHead = skinColor.getPrincessHead();
        return "⠀ ⠀ ⠀  " + princessHead + "         " + raisedHand + "\n" +
                "　   " + e + e + e + e + "\n" +
                "    " + e + "   " + e + "\n" +
                "   " + downwardPointingHand + "  " + e + e + " \n" +
                "  　 " + e + e + e + "\n" +
                "　 " + e + e + e + e + "\n" +
                "　    👠     👠 \n" +
                greeting + " the princess of " + emoji.getName();
    }
}
