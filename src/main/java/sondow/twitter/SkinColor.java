package sondow.twitter;

public enum SkinColor {
    DEFAULT("🤚", "👇", "👸"),
    LIGHT("🤚🏻", "👇🏻", "👸🏻"),
    MEDIUM_LIGHT("🤚🏼", "👇🏼", "👸🏼"),
    MEDIUM("🤚🏽", "👇🏽", "👸🏽"),
    MEDIUM_DARK("🤚🏾" , "👇🏾", "👸🏾"),
    DARK("🤚🏿" , "👇🏿", "👸🏿")
    ;

    private final String raisedHand;
    private final String downwardPointingHand;
    private final String princessHead;

    private SkinColor(String raisedHand, String downwardPointingHand, String princessHead) {
        this.raisedHand = raisedHand;
        this.downwardPointingHand = downwardPointingHand;
        this.princessHead = princessHead;
    }

    public String getRaisedHand() {
        return raisedHand;
    }

    public String getDownwardPointingHand() {
        return downwardPointingHand;
    }

    public String getPrincessHead() {
        return princessHead;
    }
}
