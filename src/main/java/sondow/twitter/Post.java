package sondow.twitter;

import java.util.Objects;

public class Post {

    /**
     * Mastodon has a content warning feature that lets you hide the body of the post behind a
     * description of the contents. Since people using screen-readers could get annoyed hearing the
     * contents of a long post that's just a series of emojis, we use a short description of the
     * post as the content warning or subject of the post.
     */
    private String shortDescription;

    /**
     * The message of the post, containing all the emojis and whitespace that will be displayed.
     */
    private String bodyText;

    public Post(String shortDescription, String bodyText) {
        this.shortDescription = shortDescription;
        this.bodyText = bodyText;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getBodyText() {
        return bodyText;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Post post = (Post) o;
        return Objects.equals(shortDescription, post.shortDescription) &&
                Objects.equals(bodyText, post.bodyText);
    }

    @Override public int hashCode() {
        return Objects.hash(shortDescription, bodyText);
    }
}
