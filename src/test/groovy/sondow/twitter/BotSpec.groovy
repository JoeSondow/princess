package sondow.twitter

import java.security.SecureRandom
import spock.lang.Specification

class BotSpec extends Specification {

    def "go should make tweeter post a tweet"() {
        setup:
        Tooter tooter = Mock()
        byte[] byteArray = [0x0, 0x1, 0x3] as byte[]
        SecureRandom random = new SecureRandom(byteArray)
        Bot bot = new Bot(tooter, random)
        String message = '' +
                '⠀ ⠀ ⠀  👸🏻         🤚🏻\n' +
                '　   🥓🥓🥓🥓\n' +
                '    🥓   🥓\n' +
                '   👇🏻  🥓🥓 \n' +
                '  　 🥓🥓🥓\n' +
                '　 🥓🥓🥓🥓\n' +
                '　    👠     👠 \n' +
                'kneel before me. i\'m the princess of bacon'
        Post post = new Post("Emoji art" , message)

        when:
        bot.go()

        then:
        1 * tooter.toot(post)
        0 * _._
    }
}
